
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity debouncer is
    Port ( 
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           btn_in : in STD_LOGIC;
           btn_out : out STD_LOGIC);
end debouncer;

architecture Behavioral of debouncer is
 signal Q1, Q2, Q3, Q4, Q5,Q6: std_logic;
 
begin
process(clk)
 begin
 if (clk'event and clk = '1') then
  if (rst = '1') then
    Q1 <= '0';
    Q2 <= '0';
    Q3 <= '0';
    Q4 <= '0';
    Q5<= '0';
    Q6<= '0';
  else
    Q1 <= btn_in;
    Q2 <= Q1;
    Q3 <= Q2;
    Q4 <= Q3;
    Q5 <= Q4;
    Q6 <= Q5;
  end if;
 end if;
end process;
 
  btn_out <= Q1 and Q2 and Q3 and Q4 and Q5 and not(Q6);
end Behavioral;
