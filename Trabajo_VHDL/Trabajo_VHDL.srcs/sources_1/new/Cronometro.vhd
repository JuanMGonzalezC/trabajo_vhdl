
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Cronometro is
    Port (
        clk : in std_logic;
        rst : in std_logic;
        starstop : in std_logic;
        Boton_Lap: in STD_LOGIC;
        lap_inc: in STD_LOGIC;
        lap_dec: in STD_LOGIC;
        display_number: out std_logic_vector(6 downto 0);
        display_selection : out std_logic_vector (7 downto 0);
        punto_7seg: out std_logic;
        SonidoBoton: buffer std_logic
    
     );
end Cronometro;

architecture Behavioral of Cronometro is

    component Display_Refresh is
        PORT(
             segment_unid_sec : in STD_LOGIC_VECTOR (6 downto 0);
             segment_unid_min : in STD_LOGIC_VECTOR (6 downto 0);
             segment_dec_sec : in STD_LOGIC_VECTOR (6 downto 0);
             segment_dec_min : in STD_LOGIC_VECTOR (6 downto 0);
             segment_unid_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
             segment_unid_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
             segment_dec_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
             segment_dec_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
             clk : in STD_LOGIC;
             Display_number : out STD_LOGIC_VECTOR (6 downto 0);
             Display_select : out STD_LOGIC_VECTOR (7 downto 0)              
            );
       end component;
       
       component counter is
          generic (maximo:integer:=10);
           Port ( reset : in STD_LOGIC;
                  clk : in STD_LOGIC;
                  enable : in STD_LOGIC;
                  count : out STD_LOGIC_VECTOR (3 downto 0);
                  salida : out STD_LOGIC);
       end component;

        component decoder is
             Port ( 
                code : IN std_logic_vector(3 DOWNTO 0);
                led : OUT std_logic_vector(6 DOWNTO 0)
             );
            end component;
            
         component div_frec is
                 generic (frecuencia: integer := 500000 );--
                 PORT (
                     RESET : in std_logic;
                     CLK_IN : in std_logic;
                     CLK_OUT: out std_logic
                 );
                end component;
          component maquina_estados
             Port ( 
                    pulsador : in STD_LOGIC;
                    reset_i : in STD_LOGIC;
                    clk_i: in STD_LOGIC;
                    enable: out STD_LOGIC;
                    reset_o : out STD_LOGIC;
                    reset_sonido : out STD_LOGIC
                );          
                end component;
                
         component debouncer 
             Port ( 
                     clk : in STD_LOGIC;
                     rst : in STD_LOGIC;
                     btn_in : in STD_LOGIC;
                     btn_out : out STD_LOGIC);
                 end component;
                 
         component sincronizador is
            Port (
            sync_in: IN STD_LOGIC;
            clk: IN STD_LOGIC;
            sync_out: OUT STD_LOGIC
            );
            end component;
            
            
          component Sonido is
                Port ( reset : in STD_LOGIC;
                       clk : in STD_LOGIC;
                       Sonido : buffer std_logic);
            end component;

   signal rst_global: std_logic:='0';
   signal enable_global: std_logic := '0';
   signal clk_1s: std_logic:='0';
   signal clk_disp: std_logic:='0';
   signal clk_maq: std_logic;
   
   signal of_1: std_logic;
   signal of_2: std_logic;
   signal of_3: std_logic;
   signal of_4: std_logic;
   
   signal cont_seg_uni : std_logic_vector (3 downto 0);
   signal cont_seg_dec : std_logic_vector (3 downto 0);
   signal cont_min_uni : std_logic_vector (3 downto 0);  
   signal cont_min_dec : std_logic_vector (3 downto 0);  
   
   signal segment_unid_sec : STD_LOGIC_VECTOR (6 downto 0);
   signal segment_unid_min : STD_LOGIC_VECTOR (6 downto 0);
   signal segment_dec_sec : STD_LOGIC_VECTOR (6 downto 0);  
   signal segment_dec_min : STD_LOGIC_VECTOR (6 downto 0);
   
   type lap is array (3 downto 0) of std_logic_vector (6 downto 0) ;
    signal lap_0: lap := (others => "1111110");
    signal lap_1: lap:= (others => "1111101");
    signal lap_2: lap:= (others => "1111011");
    signal lap_3: lap:= (others => "1110111");
    
    signal segment_unid_sec_lap : STD_LOGIC_VECTOR (6 downto 0):="1111110";
   signal segment_unid_min_lap : STD_LOGIC_VECTOR (6 downto 0):="1111101";
   signal segment_dec_sec_lap : STD_LOGIC_VECTOR (6 downto 0):="1111011";  
   signal segment_dec_min_lap : STD_LOGIC_VECTOR (6 downto 0):="1110111";        
   
    TYPE state_type IS (S0, S1, S2);
    SIGNAL state:state_type := s0;
    SIGNAL next_state: state_type := s0;  
     
-- Se�ales de deteccion de flanco del boton lap
    signal boton_lap_deb : std_logic := '0';
    signal flanco_d : std_logic := '0';
    signal flanco_re : std_logic := '0';
    
    signal lap_inc_deb : std_logic := '0';
    signal flanco_inc_d : std_logic := '0';
    signal flanco_inc_re : std_logic := '0';
    
     signal lap_dec_deb : std_logic := '0';
    signal flanco_dec_d : std_logic := '0';
    signal flanco_dec_re : std_logic := '0';
-- Se�al Boton depurado
    signal boton_salida : std_logic :='0';
-- Se�al de sincronizaci�n
    signal inc_sync : std_logic :='0';
    signal dec_sync : std_logic :='0';    
 --buffer del canal de seleccion
    signal selecc_buff: std_logic_vector (7 downto 0);
    
    signal son_bot: std_logic;
    
    
begin
--INSTACIACI�N

    --MAQUINA ESTADOS   
      maq_estado: maquina_estados Port MAP( 
          pulsador => boton_salida,
          reset_i => rst,
          clk_i => clk,
          enable => enable_global,
          reset_o => rst_global,
          reset_sonido => son_bot
      ); 

    --DIVISOR DE FRECUENCIA 1 segundo
      div_frec_1: div_frec generic map (frecuencia=>50000000) PORT MAP (
         RESET=>rst_global,
         CLK_IN=>clk,
         clk_out=>clk_1s
        );
 
        
    --DIVISOR DE FRECUENCIA display
        div_frec_disp: div_frec  generic map (frecuencia=>100000)  PORT MAP (
            RESET=>rst_global,
             CLK_IN=>clk,
             clk_out=>clk_disp
          );
          
 --DISPLAY REFRESH 
       Disp_refre: Display_Refresh PORT MAP (
            segment_unid_sec =>segment_unid_sec,
            segment_unid_min =>segment_unid_min,
             segment_dec_sec =>segment_dec_sec,
             segment_dec_min =>segment_dec_min,
            segment_unid_sec_lap =>segment_unid_sec_lap,
            segment_unid_min_lap =>segment_unid_min_lap,
             segment_dec_sec_lap =>segment_dec_sec_lap,
             segment_dec_min_lap =>segment_dec_min_lap,
             clk => clk_disp,
             Display_number  => display_number,
             Display_select  => selecc_buff
        );   
          
          
    --CONTADORES        
      cont_1: counter generic map (maximo=>10) port map (
             reset=>rst_global,
             clk=>clk_1s,
             enable=>enable_global,
             count=>cont_seg_uni,
             salida=>of_1
        );
        
 
      
       cont_2: counter generic map (maximo=>6) port map (
             reset=>rst_global,
             clk=>of_1,
             enable=>enable_global,
             count=>cont_seg_dec,
             salida=>of_2
        );
     	
        cont_3: counter generic map (maximo=>10) port map (
             reset=>rst_global,
             clk=>of_2,
             enable=>enable_global,
             count=>cont_min_uni,
             salida=>of_3
        );
      

        cont_4: counter generic map (maximo=>10) port map (
              reset=>rst_global,
              clk=>of_3,
              enable=>enable_global,
              count=>cont_min_dec,
              salida=>of_4
        );

    
        
    --DECODIFICACODRES BINARIO-7SEG    
       Deco_1: decoder PORT MAP(
        code => cont_seg_uni,
        led => segment_unid_sec
        );
        
      Deco_2: decoder PORT MAP(
        code => cont_seg_dec,
        led => segment_dec_sec
        );
        
       Deco_3: decoder PORT MAP(
        code => cont_min_uni,
        led => segment_unid_min
        );
        
       Deco_4: decoder PORT MAP(
        code => cont_min_dec,
        led => segment_dec_min
        );

     -- DEBOUNCER
       deboun: debouncer Port MAP( 
         clk => clk,
         rst => rst,
         btn_in => starstop,
         btn_out => boton_salida
         );
         
         deboun_lap: debouncer Port MAP( 
         clk => clk,
         rst => rst,
         btn_in => boton_lap,
         btn_out => boton_lap_deb
         ); 
         
          deboun_lap_inc: debouncer Port MAP( 
         clk => clk,
         rst => rst,
         btn_in => inc_sync,
         btn_out => lap_inc_deb
         ); 
         
          deboun_lap_dec: debouncer Port MAP( 
         clk => clk,
         rst => rst,
         btn_in => dec_sync,
         btn_out => lap_dec_deb
         ); 
         
         sincro_inc: sincronizador  Port MAP(
            sync_in => lap_inc ,
            clk =>  clk,
            sync_out =>  inc_sync
            );
            
          sincro_dec: sincronizador  Port MAP(
            sync_in => lap_dec ,
            clk => clk ,
            sync_out =>  dec_sync
            );
            
            
          sonido_1: Sonido  Port MAP(
              clk => clk ,
              reset => son_bot ,
              Sonido =>  SonidoBoton
              );
         
         
   proc_lap_captura: process(flanco_re,clk,rst_global)
   variable conta: integer range 0 to 3:=0;
    begin
        if rst_global='1' then
         lap_0 <=(others => "1111110");
         lap_1 <= (others => "1111101");
         lap_2 <= (others => "1111011");
         lap_3 <= (others => "1110111");
    
        elsif(flanco_re ='1' and rising_edge(clk) ) then
       
   if conta = 0 then
        lap_0(0) <= segment_unid_sec;
        lap_0(1) <= segment_unid_min;
        lap_0(2) <=  segment_dec_sec;
        lap_0(3) <=  segment_dec_min; 
        conta := conta +1;    
    elsif conta = 1 then
        lap_1(0) <= segment_unid_sec;
        lap_1(1) <= segment_unid_min;
        lap_1(2) <=  segment_dec_sec;
        lap_1(3) <=  segment_dec_min;  
        conta := conta +1;    
     elsif conta = 2 then
        lap_2(0) <= segment_unid_sec;
        lap_2(1) <= segment_unid_min;
        lap_2(2) <=  segment_dec_sec;
        lap_2(3) <=  segment_dec_min;   
        conta := conta +1;    
     elsif conta = 3 then    
        lap_3(0) <= segment_unid_sec;
        lap_3(1) <= segment_unid_min;
        lap_3(2) <=  segment_dec_sec;
        lap_3(3) <=  segment_dec_min;
        conta := 0;     
         end if;          
        end if;      
   end process;

   proc_lap_cambio: process(flanco_dec_re,flanco_inc_re,clk)
   variable cont_cam: unsigned (1 downto 0) :="00";
    begin
    
    
    
   if  rising_edge(clk) then
    
        if( flanco_dec_re ='1'  ) then 
            cont_cam:= cont_cam -1;
    
        elsif( flanco_inc_re = '1'  ) then 
            cont_cam:= cont_cam +1;            
         
        end if;
        
     if cont_cam = "00" then     
      segment_unid_sec_lap  <=   lap_0(0);
      segment_unid_min_lap <=    lap_0(1);
      segment_dec_sec_lap   <=   lap_0(2);
      segment_dec_min_lap  <=    lap_0(3);
     
     elsif cont_cam = "01" then    
      segment_unid_sec_lap  <=   lap_1(0);
      segment_unid_min_lap <=    lap_1(1);
      segment_dec_sec_lap   <=   lap_1(2);
      segment_dec_min_lap  <=    lap_1(3);
            
     elsif cont_cam = "10" then
      segment_unid_sec_lap  <=   lap_2(0);
      segment_unid_min_lap <=    lap_2(1);
      segment_dec_sec_lap   <=   lap_2(2);
      segment_dec_min_lap  <=    lap_2(3);
            
     elsif cont_cam = "11" then
      segment_unid_sec_lap  <=   lap_3(0);
      segment_unid_min_lap <=    lap_3(1);
      segment_dec_sec_lap   <=   lap_3(2);
      segment_dec_min_lap  <=    lap_3(3);
               
    end if; 
    end if;          
   end process;
   


flanco_d <= boton_lap_deb when rising_edge(clk);
flanco_re <= not flanco_d and boton_lap_deb;

flanco_inc_d <= lap_inc_deb when rising_edge(clk);
flanco_inc_re <= (not flanco_inc_d) and lap_inc_deb;

flanco_dec_d <= lap_dec_deb when rising_edge(clk);
flanco_dec_re <= (not flanco_dec_d) and lap_dec_deb;

display_selection <=  selecc_buff;
punto_7seg <= '0' when selecc_buff ="10111111" else
               '0' when selecc_buff ="11111011" else
               '1';

end Behavioral;
