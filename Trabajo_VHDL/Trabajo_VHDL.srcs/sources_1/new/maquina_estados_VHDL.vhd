library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity maquina_estados is
    Port ( pulsador : in STD_LOGIC;
           reset_i : in STD_LOGIC;
           clk_i: in STD_LOGIC;
           enable: out STD_LOGIC;
           reset_o : out STD_LOGIC;
           reset_sonido : out STD_LOGIC);          
end maquina_estados;

architecture Behavioral of maquina_estados is
--Se�ales de los estados 
    TYPE state_type IS (S0, S1, S2);
    SIGNAL state:state_type := S0;
    SIGNAL next_state: state_type;  
-- Se�ales de deteccion de flanco del startstop       
    signal flanco_d : std_logic := '0';
    signal flanco_re : std_logic := '0';
BEGIN
process(clk_i,reset_i)	begin
	if reset_i='1' then
		state <=S0;
	elsif (clk_i'event and clk_i = '1') then
		state<=next_state;
	end if;
 end process;



process(flanco_re, reset_i)
            begin

      case state is	
        when S0 =>
        		if  reset_i= '1' then next_state<=S0;
        		elsif (flanco_re ='1')  then next_state<=S1;
        		else next_state<=S0;
        		end if;
        	
        when S1=>
                if  reset_i= '1' then next_state<=S0;
        		elsif (flanco_re ='1') then next_state<=S2;
                else next_state<=S1;
        		end if;
        	
        when S2=>
                if  (reset_i= '1') then next_state<=S0;
        		elsif (flanco_re ='1') then next_state<=S1;		
        		else next_state<=S2;
        		end if;
        	
        	    
        WHEN OTHERS =>
                next_state <= S0;
      end case;

end process;


process(state) begin
    case state is
        when s0=> 
            reset_o <= '1'; 
            enable <= '0';
            reset_sonido <= '0';
        
        when s1=>
            reset_o<='0'; 
            enable<='0';
            reset_sonido <= '1';
        
        when s2=>
            reset_o<='0'; 
            enable<='1';
            reset_sonido <= '1';
        

        when others =>
            reset_o<='1';   
            enable<='0';
            reset_sonido <= '1';
            
    end case;
end process;

flanco_d <= pulsador when rising_edge(clk_i);
flanco_re <= not flanco_d and pulsador;

end Behavioral;