----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.01.2019 13:26:58
-- Design Name: 
-- Module Name: Sonido - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.AlL;
use IEEE.STD_LOGIC_unsigned.AlL;

entity Sonido is
  Port ( clk, reset : in std_logic;
  Sonido : buffer std_logic);
end Sonido;
architecture Behavioral of Sonido is

signal contador: integer range 0 to 170265;
signal bandera: integer range 0 to 80e5;
begin
process(clk, reset)
begin
if reset = '1' then
    contador <= 0;
    Sonido <='0';
    bandera<= 0;
elsif rising_edge(clk) and (bandera<80e5) then 
    bandera<= bandera+1; 
    if contador = 170265 then
        contador <=0;
        Sonido <= not Sonido;
    else 
        contador <= contador +1;
    end if;    
end if;
end process;
end Behavioral;


