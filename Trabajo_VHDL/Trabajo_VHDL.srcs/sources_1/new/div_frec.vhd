library IEEE;
use IEEE.std_logic_1164.all;

entity div_frec is
 generic (
   frecuencia: integer := 50000000 
 );
 port (
  RESET : in std_logic;
  CLK_IN : in std_logic;
  CLK_OUT: out std_logic
 );
end div_frec;

architecture behavioral of div_frec is
  signal clk_signal:std_logic;
begin
  process (RESET, CLK_IN)
  variable count:integer;
 begin
   if RESET = '1' then
     count := 0;
     clk_signal <= '0';
   elsif CLK_IN'event and CLK_IN ='1' then
     if count/=frecuencia then
        count := count+1;
     else
        count := 0;
        clk_signal<=not(clk_signal);
      end if;
     end if;
 end process;
 CLK_OUT<=clk_signal;
end behavioral;
