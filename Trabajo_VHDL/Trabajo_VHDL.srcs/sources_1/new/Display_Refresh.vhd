
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Display_Refresh is
    Port ( segment_unid_sec : in STD_LOGIC_VECTOR (6 downto 0);
           segment_unid_min : in STD_LOGIC_VECTOR (6 downto 0);
           segment_dec_sec : in STD_LOGIC_VECTOR (6 downto 0);
           segment_dec_min : in STD_LOGIC_VECTOR (6 downto 0);
           segment_unid_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
           segment_unid_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
           segment_dec_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
           segment_dec_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
           clk : in STD_LOGIC;
           Display_number : out STD_LOGIC_VECTOR (6 downto 0);
           Display_select : out STD_LOGIC_VECTOR (7 downto 0));
end Display_Refresh;

architecture Behavioral of Display_Refresh is

 begin
 process(clk, segment_unid_sec, segment_unid_min, segment_dec_sec, segment_dec_min)
   variable dis_sel : std_logic_vector(Display_select'length - 1 downto 0) := "11111110";
    begin
    if rising_edge(clk) then
        dis_sel := to_stdlogicvector(to_bitvector(dis_sel) rol 1);
    end if; 
    case dis_sel is
            when "11111110" => Display_number <= segment_unid_sec;
            when "11111101" => Display_number <= segment_dec_sec;
            when "11111011" => Display_number <= segment_unid_min;
            when "11110111" => Display_number <= segment_dec_min;
            when "11101111" => Display_number <= segment_unid_sec_lap;
            when "11011111" => Display_number <= segment_dec_sec_lap;
            when "10111111" => Display_number <= segment_unid_min_lap;
            when "01111111" => Display_number <= segment_dec_min_lap;
            when others => report "unreachable" severity failure;
        end case;
    Display_select <= dis_sel;
 end process;

end Behavioral;