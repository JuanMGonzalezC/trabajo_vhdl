

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity counter is

    generic (maximo:integer:=10);
    Port ( reset : in STD_LOGIC;
           clk : in STD_LOGIC;
           enable : in STD_LOGIC;
           count : out STD_LOGIC_VECTOR (3 downto 0);
           salida : out STD_LOGIC);
end counter;

architecture Behavioral of counter is
signal count_i: unsigned(count'length -1 downto 0):="0000";
begin
process (enable,clk,reset)
    begin 
    if reset= '1' then
    count_i<="0000";
    salida<='0';
    elsif rising_edge(clk) and enable= '1' then
        if (count_i = maximo - 1) then
         salida<='1';
         count_i<="0000";
        else
         count_i <= unsigned(count_i) + 1;
         salida<='0';
        end if;
    end if;
 end process;
 count<=std_logic_vector(count_i);
end Behavioral;