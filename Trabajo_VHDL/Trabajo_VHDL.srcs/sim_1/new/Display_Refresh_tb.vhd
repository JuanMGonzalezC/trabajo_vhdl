
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Display_Refresh_tb is
--  Port ( );
end Display_Refresh_tb;

architecture Behavioral of Display_Refresh_tb is

    COMPONENT Display_Refresh
    PORT(
         segment_unid_sec : in STD_LOGIC_VECTOR (6 downto 0);
         segment_unid_min : in STD_LOGIC_VECTOR (6 downto 0);
         segment_dec_sec : in STD_LOGIC_VECTOR (6 downto 0);
         segment_dec_min : in STD_LOGIC_VECTOR (6 downto 0);
         segment_unid_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
         segment_unid_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
         segment_dec_sec_lap : in STD_LOGIC_VECTOR (6 downto 0);
         segment_dec_min_lap : in STD_LOGIC_VECTOR (6 downto 0);
         clk : in STD_LOGIC;
         Display_number : out STD_LOGIC_VECTOR (6 downto 0);
         Display_select : out STD_LOGIC_VECTOR (7 downto 0)
        );
    END COMPONENT;
    
       --Inputs
    signal clk : std_logic := '0';
    signal segment_unid_sec : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_unid_min : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_dec_sec : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_dec_min : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_unid_sec_lap : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_unid_min_lap : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_dec_sec_lap : std_logic_vector(6 downto 0) := (others => '0');
    signal segment_dec_min_lap : std_logic_vector(6 downto 0) := (others => '0');
    
      --Outputs
    signal Display_number : std_logic_vector(6 downto 0);
    signal Display_select : std_logic_vector(7 downto 0);
 
    -- Clock period definitions
    constant clk_period : time := 100 ns;
    
begin
	-- Instantiate the Unit Under Test (UUT)
uut: Display_Refresh PORT MAP (
       segment_unid_sec => segment_unid_sec,
       segment_unid_min => segment_unid_min,
       segment_dec_sec => segment_dec_sec,
       segment_dec_min => segment_dec_min,
       segment_unid_sec_lap => segment_unid_sec_lap,
       segment_unid_min_lap => segment_unid_min_lap,
       segment_dec_sec_lap => segment_dec_sec_lap,
       segment_dec_min_lap => segment_dec_min_lap,
       clk => clk,
       Display_number =>Display_number,
       Display_select =>Display_select
    );

-- Clock process definitions
clk_process :process
begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
end process;


-- Stimulus process
stim_proc: process
begin        
  -- hold reset state for 100 ns.
  wait for 100 ns;    
 segment_unid_sec <= "0000000";
 segment_unid_min <= "0000001";
 segment_dec_sec <= "0000011";
 segment_dec_min <= "0000111";
 segment_unid_sec_lap <= "0001111";
 segment_unid_min_lap <= "0011111";
 segment_dec_sec_lap <= "0111111";
 segment_dec_min_lap <= "1111111";
 
 wait for 100 ns;
 segment_unid_sec <= to_stdlogicvector(to_bitvector(segment_unid_sec) rol 1);
 segment_unid_min <= to_stdlogicvector(to_bitvector(segment_unid_min) rol 1);
 segment_dec_sec <= to_stdlogicvector(to_bitvector(segment_dec_sec) rol 1);
 segment_dec_min <= to_stdlogicvector(to_bitvector(segment_dec_min) rol 1);
 segment_unid_sec_lap <= to_stdlogicvector(to_bitvector(segment_unid_sec_lap) rol 1);
 segment_unid_min_lap <= to_stdlogicvector(to_bitvector(segment_unid_min_lap) rol 1);
 segment_dec_sec_lap <= to_stdlogicvector(to_bitvector(segment_dec_sec_lap) rol 1);
 segment_dec_min_lap <= to_stdlogicvector(to_bitvector(segment_dec_min_lap) rol 1);
 
 wait for 100 ns;
 segment_unid_sec <= to_stdlogicvector(to_bitvector(segment_unid_sec) rol 1);
 segment_unid_min <= to_stdlogicvector(to_bitvector(segment_unid_min) rol 1);
 segment_dec_sec <= to_stdlogicvector(to_bitvector(segment_dec_sec) rol 1);
 segment_dec_min <= to_stdlogicvector(to_bitvector(segment_dec_min) rol 1);
 segment_unid_sec_lap <= to_stdlogicvector(to_bitvector(segment_unid_sec_lap) rol 1);
 segment_unid_min_lap <= to_stdlogicvector(to_bitvector(segment_unid_min_lap) rol 1);
 segment_dec_sec_lap <= to_stdlogicvector(to_bitvector(segment_dec_sec_lap) rol 1);
 segment_dec_min_lap <= to_stdlogicvector(to_bitvector(segment_dec_min_lap) rol 1);
end process;

end Behavioral;
