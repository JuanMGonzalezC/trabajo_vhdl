
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity div_frec_tb is
--  Port ( );
end div_frec_tb;

architecture Behavioral of div_frec_tb is

component div_frec is
port(
 RESET : in std_logic;
 CLK_IN : in std_logic;
 CLK_OUT: out std_logic
);
end component;
 signal reset : std_logic:= '0';
 signal clk_in : std_logic:= '0';
 signal clk_out : std_logic:= '0';


constant clk_period:time:=5ns;

begin

uut: div_frec PORT MAP (
 clk_in=>CLK_IN,
 reset=>RESET,
 clk_out=>CLK_OUT
);

clk_process:process
begin
    clk_in<='0';
    wait for clk_period;
    clk_in<='1';
    wait for clk_period;
end process;


stim_process:process
begin
    wait for 100 ns;
    reset<='1';
    wait for 500 ns;
    reset<='0';
    wait for 5000 ms;
    wait;
end process;
end Behavioral;
