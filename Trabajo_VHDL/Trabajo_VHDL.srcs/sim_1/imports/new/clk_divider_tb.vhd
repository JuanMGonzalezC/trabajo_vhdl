----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.12.2018 16:04:15
-- Design Name: 
-- Module Name: clk_divider_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider_tb is
--  Port ( );
end clk_divider_tb;

architecture Behavioral of clk_divider_tb is

component clk_divider is
port(
 RESET : in std_logic;
 CLK_IN : in std_logic;
 CLK_OUT: out std_logic
);
end component;
 signal reset : std_logic:= '0';
 signal clk_in : std_logic:= '0';
 signal clk_out : std_logic:= '0';


constant clk_period:time:=5ns;

begin

uut: clk_divider PORT MAP (
 clk_in=>CLK_IN,
 reset=>RESET,
 clk_out=>CLK_OUT
);

clk_process:process
begin
    clk_in<='0';
    wait for clk_period;
    clk_in<='1';
    wait for clk_period;
end process;


stim_process:process
begin
    wait for 100 ns;
    reset<='1';
    wait for 100 ns;
    reset<='0';
    wait;
end process;
end Behavioral;
